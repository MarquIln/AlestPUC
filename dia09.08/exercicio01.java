import java.util.Scanner;

public class exercicio01 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Digite o valor do veículo: ");
        double value = in.nextInt();

        System.out.println("Digite o tipo de combustível: ");
        System.out.println(" 1 - Gasolina \n 2 - Diesel \n 3 - Álcool");
        int typeGas = in.nextInt();
        valorVeiculo(value, typeGas);
    }

    public static void valorVeiculo (double value, int typeGas) {
        double desconto = 0;
        double semDesconto = value;
        if (typeGas == 1) {
            desconto = value * 0.21;
            value = value * 0.79;
        } else if (typeGas == 2) {
            desconto = value * 0.14;
            value = value * 0.86;
        } else if (typeGas == 3) {
            desconto = value * 0.25;
            value = value * 0.75;
        } else {
            System.out.println("tipo de gasolina errado.");
        }
        System.out.println("o valor do veículo sem desconto é: " + semDesconto);
        System.out.println("O desconto do veículo é: " + desconto);
        System.out.println("O valor do veículo é: " + value);
    }
}