import java.util.Random;

public class exercicio03 {
    public static void main(String[] args) {
        Random rdm = new Random();

        int columns = 5;
        int rows = 4;
        int[][] array = new int[columns][rows];

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                array[i][j] = rdm.nextInt(100);
            }
        }

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                System.out.println(array[i][j] + "\t");
            }
            System.out.println();
        }
    }
}

/*
    Construa uma matriz de 5x4 que em cada posição receba um número randômico entre 0 e 99. 
    Exiba a matriz gerada de maneira adequada na tela. 
    Depois, crie um vetor que armazene os valores de todos os números pares da matriz e exiba o resultado do vetor.
     Use a classe Random para geração de valores aleatórios.
        import java.util.Random;
        Random rdm = new Random();
        int valor = rdm.nextInt(100);
*/