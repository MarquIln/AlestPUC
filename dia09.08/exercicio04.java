import java.util.Random;
import java.util.Scanner;

public class exercicio04 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rdm = new Random();

        int numeroSecreto = rdm.nextInt(50);

        System.out.println("Digite um numero inteiro: ");

        int numeroSolicitado = in.nextInt();
        int tentativas = 0;
        int maxTentativas = 10;
        boolean acertou = false;

        while (tentativas < maxTentativas) {
            tentativas++;
            if (numeroSecreto > numeroSolicitado) {
                System.out.println("O numero secreto eh MAIOR");
            } else if (numeroSecreto < numeroSolicitado) {
                System.out.println("O numero secreto eh MENOR");
            } else {
                acertou = true;
                System.out.println("Parabens, voce acertou o numero em " + tentativas + " tentativas.");
                break;
            }
            numeroSolicitado = in.nextInt();
        }

        if (!acertou) {
            System.out.println("Boa sorte na proxima!");
        }
    }
}

/*
    Crie pequeno jogo de adivinhação onde inicialmente deve-se gerar um número aleatório inteiro entre 0 e 50 que 
    será o "número secreto". Em seguida solicite que o usuário digite um número inteiro.
    Se o número digitado for menor que o número secreto, imprima “O número secreto é MAIOR”, 
    se o número digitado for maior que o número secreto, imprima “O número secreto é MENOR”.
    O usuário deve ter 10 tentativas de digitação de um número. Caso em algum momento ele acerte, 
    o sistema deve apresentar a mensagem "Parabéns, você acertou em X tentativas!" 
    onde X é o número de tentativas que ele levou para acertar e encerrar a execução.
    Caso o usuário não consiga acertar nas 10 tentativas, apresente uma mensagem desejando "Boa sorte na próxima!". 
*/
