import java.util.Scanner;

public class exercicio02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] vetor = new int[10];
        for (int i = 0; i < 10; i++) {
            vetor[i] = scanner.nextInt();
        }
        for (int i = 9; i >= 0; i--) {
            System.out.print(vetor[i] + " ");
        }
        System.out.println(); 

        System.out.print("Digite um número inteiro para procurar no vetor: ");
        int numeroProcurado = scanner.nextInt();

        boolean encontrado = false;
        int indiceEncontrado = -1;
        for (int i = 0; i < 10; i++) {
            if (vetor[i] == numeroProcurado) {
                encontrado = true;
                indiceEncontrado = i;
                break;
            }
        }

        if (encontrado) {
            System.out.println("Número " + numeroProcurado + " encontrado no índice " + indiceEncontrado + ".");
        } else {
            System.out.println("Valor " + numeroProcurado + " não encontrado.");
        }
    }
}
