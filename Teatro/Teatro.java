class Teatro {
    private int filas = 8;
    private int poltronas = 10;
    private int[][] mapa;
    private int reservados = 0;
    private int naoAlocados = 0;

    public Teatro() {
        this.mapa = new int[filas][poltronas];
    }

    public void mostraMapa() {
        for (int i = 0; i < mapa.length; i++) {
            for (int j = 0; j < mapa[i].length; j++) {
                System.out.print(mapa[i][j] == 0 ? " 0 " : " X ");
            }
            System.out.println();
        }
    }

    public void fazerReserva(int fila, int poltrona) {
        if (fila < 1 || fila > filas && poltrona < 1 || poltrona > poltronas) {
            System.out.println("Opcao Invalida!");
            return;
        }
        if (mapa[fila - 1][poltrona - 1] == 1) {
            System.out.println("Poltrona ja reservada");
            return;
        }
        mapa[fila - 1][poltrona - 1] = 1;
        System.out.println("Reserva feita para a fila " + fila + " na poltrona " + poltrona + ".");
    }    

    public void statusOcupacao() {
        for (int[] fila : mapa) {
            for (int poltrona : fila) {
                if (poltrona == 1) {
                    reservados++;
                } else
                    naoAlocados++;
            }
        }
        System.out.println(reservados);
        System.out.println(naoAlocados);
    }

    public void verificaCadeira(int fila, int poltrona) {
        if (fila < 1 || fila > filas || poltrona < 1 || poltrona > poltronas) {
            System.out.println("Fila ou poltrona inválida.");
            return;
        }
        String status = (mapa[fila - 1][poltrona - 1] == 1) ? "ocupada" : "livre";
        System.out.println("A poltrona " + poltrona + " na fila " + fila + " está " + status);
    }   
}
