import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Teatro teatro = new Teatro();
        Scanner in = new Scanner(System.in);

        while (true) {
            System.out.println("Opções:");
            System.out.println("1) Mostrar mapa");
            System.out.println("2) Fazer reserva");
            System.out.println("3) Status de ocupação");
            System.out.println("4) Verificação de poltrona");
            System.out.println("0) Sair");
            
            int opcao = in.nextInt();
            
            switch (opcao) {
                case 1:
                    teatro.mostraMapa();
                    System.out.println();
                    break;
                case 2:
                while (true) {
                    System.out.print("Digite o número da fila: ");
                    int fila = in.nextInt();
                    System.out.print("Digite o número da poltrona: ");
                    int poltrona = in.nextInt();
                    teatro.fazerReserva(fila, poltrona);
                    System.out.println("Opções:");
                    System.out.println("1) Fazer outra reserva");
                    System.out.println("2) Sair para o menu");
                    int escolha = in.nextInt();
                    if (escolha == 1) {
                        System.out.print("Digite o número da fila: ");
                        fila = in.nextInt();
                        System.out.print("Digite o número da poltrona: ");
                        poltrona = in.nextInt();
                        teatro.fazerReserva(fila, poltrona);
                        System.out.println();
                    } else if (escolha == 2) {
                        System.out.println();
                        break;
                    }
                    break;
                }
                case 3:
                    teatro.statusOcupacao();
                    System.out.println();
                    break;
                case 4:
                    System.out.print("Digite o número da fila: ");
                    int fila = in.nextInt();
                    System.out.print("Digite o número da poltrona: ");
                    int poltrona = in.nextInt();
                    teatro.verificaCadeira(fila, poltrona);
                    System.out.println();
                    break;
                case 0:
                    in.close();
                    System.exit(0);
                default:
                    System.out.println("Opção inválida. Tente novamente.");
            }
        }
    }
}
